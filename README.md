# Extendi's Game Of Life - Luca Panzavolta's Coding challenge

![GameOfLife Board Representation](./GameOfLifeBoard.png)

## Solution approach

All the logic and data structures responsible for holding and updating the game's state are contained in the abstract class `GameOfLife`.
As an abstract class `GameOfLife` must be extended. The `ExtendiGameOfLife` class extends the `GameOfLife` class mainly by providing the static method `initializeGame()` which is responsible for loading the .txt file with the initial game state, parsing such file and producing an instance of `ExtendiGameOfLife`.
Interacting with the instance is what allows to play the game.
There are two methods that allow to play the game:
`computeNextGeneration()`: to produce the next generation game's state
`printBoard()` to create a tabular representation of the board in the console

Check `main.ts` to see an example of how to play the game.

## How to setup the project

Running the project requires Node.js to be installed.
Run the following commands to correctly setup the project.

1. `git clone https://gitlab.com/luca.panzavolta1/extendigameoflife.git`
2. `cd extendigameoflife`
3. `npm i`

## How to play the game

After having performed the steps listed in "How to setup the project", run the game using:

`npm run playExtendiGameOfLife --file=test.txt`

This npm script will run whatever is inside `main.ts`.
You can change the file passed to the script by changing the `--file` argument.
By default you will see the game's board as it's represented by the provided file and the board for the following two generations.

N.B. I provided the npm script for semplicity's sake but the game could also be played interactively by loading up the `ExtendiGameOfLife` instance in the node.js console and interacting with it with the methods: `computeNextGeneration()` and `printBoard()`.

## Next steps

Things that could be added to the codebase to improve it:

1. dockerize the application
2. add a UI layer with React
3. stricter file validation and parsing checks
4. add Unit Testing
