import { Cell } from "./Cell";

export namespace Board {
  export type SIZE = [number, number];
  export type DATA = Cell[][];
  export type CELL = Cell;
  export type COORDINATES = [number, number];
  export type MATRIX = string[][];
}

export enum CellStatus {
  ALIVE = "*",
  DEAD = ".",
}
