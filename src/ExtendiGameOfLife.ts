import GameOfLife from "./GameOfLife";
import { Board, CellStatus } from "./types";
import { Cell } from "./Cell";
import * as fs from "fs";

export class ExtendiGameOfLife extends GameOfLife {
  private constructor(generation: number, boardSize: Board.SIZE, boardData: Board.DATA) {
    super(generation, boardSize, boardData);
  }

  static initializeGame(txtPath: string): GameOfLife {
    try {
      const file = fs.readFileSync(txtPath, "utf8");
      const fileLines = file.split(/\n/);

      const generation = this.getGeneration(fileLines);
      const boardSize = this.getBoardSize(fileLines);
      const boardMatrix = this.getBoardMatrix(fileLines);
      const board = this.initializeBoard(boardMatrix);

      return new ExtendiGameOfLife(generation, boardSize, board);
    } catch (err) {
      console.error("Error while loading up the .txt file provided: ", err);
      throw err;
    }
  }

  private static initializeBoard(boardMatrix: Board.MATRIX): Board.DATA {
    const board: Board.DATA = [];

    for (let row = 0; row < boardMatrix.length; row++) {
      for (let col = 0; col < boardMatrix[row].length; col++) {
        const cell = new Cell(boardMatrix[row][col] === CellStatus.ALIVE, [row, col]);
        board[row] ? board[row].push(cell) : (board[row] = [cell]);
      }
    }

    return board;
  }

  private static getGeneration(fileLines: string[]): number {
    // according to the .txt format provided
    // the generation number must be available on the file's first line
    const [firstLine] = fileLines;
    const regex = new RegExp(/\d+/, "g");
    const match = firstLine.match(regex);

    if (match) {
      const [generationAsString] = match;
      const generation = parseInt(generationAsString);
      return generation;
    }

    throw new Error("Generation not found.");
  }

  private static getBoardSize(fileLines: string[]): Board.COORDINATES {
    // according to the .txt format provided
    // board size data must be available on the file's second line
    const [_, secondLine] = fileLines;
    const regex = /\d/g;
    const match = secondLine.match(regex);

    if (match) {
      const boardSizeAsStringArray = match as [string, string];
      const boardSize = boardSizeAsStringArray.map((x) => parseInt(x)) as [number, number];
      return boardSize;
    }

    throw new Error("Board size not found.");
  }

  private static getBoardMatrix(fileLines: string[]): Board.MATRIX {
    // according to the .txt format provided
    // board matrix data must be available from the file's third line onwards
    const [_, __, ...boardRawData] = fileLines;
    const boardMatrix = boardRawData.map((row) => row.split(/\s/));
    return boardMatrix;
  }
}
