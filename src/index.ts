import { ExtendiGameOfLife } from "./ExtendiGameOfLife";

const txtFile = process.env.npm_config_file as string;

const game = ExtendiGameOfLife.initializeGame(txtFile);
game.printBoard();
game.computeNextGeneration();
game.printBoard();
game.computeNextGeneration();
game.printBoard();
