import { Board } from "./types";
import { Cell } from "./Cell";

abstract class GameOfLife {
  constructor(private _generation: number, private _boardSize: Board.SIZE, private _board: Board.DATA) {}

  public computeNextGeneration(): void {
    const nextGenerationBoard: Board.DATA = [];

    const [boardRows, boardColumns] = this.boardSize;
    for (let row = 0; row < boardRows; row++) {
      for (let col = 0; col < boardColumns; col++) {
        const cell = this.board[row][col];
        const nextCellState = this.computeNextCellState(cell);

        nextGenerationBoard[row]
          ? (nextGenerationBoard[row][col] = nextCellState)
          : (nextGenerationBoard[row] = [nextCellState]);
      }
    }

    this.board = nextGenerationBoard;
    this.generation += 1;
  }

  private computeNextCellState(cell: Board.CELL): Board.CELL {
    const aliveNeighboursCount = this.countAliveNeighbours(cell);

    let willCellBeAlive = cell.isAlive;

    if (cell.isAlive && aliveNeighboursCount < 2) willCellBeAlive = false;
    else if (cell.isAlive && aliveNeighboursCount > 3) willCellBeAlive = false;
    else if (!cell.isAlive && aliveNeighboursCount === 3) willCellBeAlive = true;

    return new Cell(willCellBeAlive, cell.coordinates);
  }

  private countAliveNeighbours(cell: Board.CELL): number {
    const neighboursCoordinates: Board.COORDINATES[] = this.getNeighboursCoordinates(cell);

    const neighboursAliveStatus = neighboursCoordinates
      .filter((neighbourCoordinates) => this.areCoordinatesValid(neighbourCoordinates))
      .map((neighbourCoordinates) => this.getCellAtCoordinates(neighbourCoordinates).isAlive);

    const aliveNeighboursCount = neighboursAliveStatus.filter(Boolean).length;

    return aliveNeighboursCount;
  }

  private getNeighboursCoordinates(cell: Board.CELL): Board.COORDINATES[] {
    const [row, col] = cell.coordinates;

    const topLeft: Board.COORDINATES = [row - 1, col - 1];
    const top: Board.COORDINATES = [row - 1, col];
    const topRight: Board.COORDINATES = [row - 1, col + 1];
    const right: Board.COORDINATES = [row, col + 1];
    const bottomRight: Board.COORDINATES = [row + 1, col + 1];
    const bottom: Board.COORDINATES = [row + 1, col];
    const bottomLeft: Board.COORDINATES = [row + 1, col - 1];
    const left: Board.COORDINATES = [row, col - 1];

    return [topLeft, top, topRight, right, bottomRight, bottom, bottomLeft, left];
  }

  private areCoordinatesValid(cellCoordinates: Board.COORDINATES): boolean {
    const [x, y] = cellCoordinates;
    const [boardX, boardY] = this.boardSize;

    return x >= 0 && y >= 0 && x < boardX && y < boardY;
  }

  private getCellAtCoordinates(cellCoordinates: Board.COORDINATES): Board.CELL {
    const [row, col] = cellCoordinates;
    return this.board[row][col];
  }

  public printBoard() {
    console.log(`==================== Gen. ${this.generation} Board ====================`);

    const result: boolean[][] = [];
    const [boardRows, boardColumns] = this.boardSize;

    for (let r = 0; r < boardRows; r++) {
      const row: boolean[] = [];
      for (let c = 0; c < boardColumns; c++) {
        const cell = this.board[r][c];
        row.push(cell.isAlive);
      }
      result.push(row);
    }

    console.table(result);
  }

  get generation() {
    return this._generation;
  }

  get boardSize() {
    return this._boardSize;
  }

  get board() {
    return this._board;
  }

  private set generation(generation: number) {
    this._generation = generation;
  }

  private set boardSize(boardSize: Board.SIZE) {
    this._boardSize = boardSize;
  }

  private set board(board: Board.DATA) {
    this._board = board;
  }
}

export default GameOfLife;
