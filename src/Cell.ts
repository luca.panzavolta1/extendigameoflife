import { Board } from "./types";

export class Cell {
  constructor(private _isAlive: boolean, private _coordinates: Board.COORDINATES) {}

  get isAlive() {
    return this._isAlive;
  }

  get coordinates() {
    return this._coordinates;
  }

  set isAlive(isAlive: boolean) {
    this._isAlive = isAlive;
  }

  set coordinates(coordinates: Board.COORDINATES) {
    this._coordinates = coordinates;
  }

  public kill() {
    this.isAlive = false;
  }

  public resuscitate() {
    this.isAlive = true;
  }
}
